package com.beatbox.ui.viewmodel;

import com.beatbox.model.BeatBox;
import com.beatbox.model.Sound;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SoundViewModelTest {
    private static final String ASSET_PATH = "assetPath";
    private BeatBox beatBox;
    private Sound sound;
    private SoundViewModel subject;

    @Before
    public void setUp() throws Exception {
        beatBox = mock (BeatBox.class);
        sound = new Sound(ASSET_PATH);
        subject = new SoundViewModel(beatBox);
        subject.setSound(sound);
    }

    @Test
    public void exposesSoundNameAsTitle (){
        assertThat(subject.getTitle(), is (sound.getName()));
    }

    @Test
    public void callsBeatBoxPLayOnButtonClicked (){
        subject.onButtonClicked ();
        verify(beatBox).playSound(sound);
    }
}
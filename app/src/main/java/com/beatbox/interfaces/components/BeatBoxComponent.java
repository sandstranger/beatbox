package com.beatbox.interfaces.components;

import com.beatbox.model.BeatBox;
import com.beatbox.modules.BeatBoxModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = BeatBoxModule.class)
@Singleton
public interface BeatBoxComponent {
    BeatBox getBeatBox();
}

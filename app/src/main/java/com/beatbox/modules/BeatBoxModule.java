package com.beatbox.modules;

import android.content.Context;

import com.beatbox.model.BeatBox;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ContextModule.class)
public class BeatBoxModule {

    @Provides
    @Singleton
    public BeatBox beatBox(Context context) {
        return new BeatBox(context);
    }
}

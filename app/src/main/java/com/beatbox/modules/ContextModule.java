package com.beatbox.modules;


import android.content.Context;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import lombok.NonNull;

@Module
public class ContextModule {
    @NonNull
    private final Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public Context context() {
        return context;
    }
}

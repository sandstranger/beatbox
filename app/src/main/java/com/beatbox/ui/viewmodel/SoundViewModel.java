package com.beatbox.ui.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.beatbox.model.BeatBox;
import com.beatbox.model.Sound;

import lombok.Getter;
import lombok.Setter;

public class SoundViewModel extends BaseObservable {
    @Getter
    private Sound sound;
    private BeatBox beatBox;

    public SoundViewModel(BeatBox beatBox) {
        this.beatBox = beatBox;
    }

    @Bindable
    public String getTitle() {
        return sound.getName();
    }

    public void setSound (Sound sound){
        this.sound = sound;
        notifyChange();
    }

    public void onButtonClicked() {
        beatBox.playSound(sound);
    }
}

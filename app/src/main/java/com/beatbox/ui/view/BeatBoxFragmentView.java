package com.beatbox.ui.view;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;

import com.beatbox.beatbox.databinding.FragmentBeatBoxBinding;
import com.beatbox.model.BeatBox;
import com.beatbox.ui.adapter.SoundAdapter;

import lombok.Builder;
import lombok.val;

@Builder
public class BeatBoxFragmentView {
    private Context context;
    private FragmentBeatBoxBinding binding;
    private BeatBox beatBox;

    public void bindView() {
        val gridColumnsCount = 3;
        binding.recyclerView.setLayoutManager(new GridLayoutManager(context, gridColumnsCount));
        binding.recyclerView.setAdapter(new SoundAdapter(beatBox.getSounds(), beatBox));
    }
}

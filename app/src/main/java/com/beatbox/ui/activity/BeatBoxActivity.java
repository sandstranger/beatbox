package com.beatbox.ui.activity;


import android.support.v4.app.Fragment;

import com.beatbox.ui.fragment.BeatBoxFragment;

public class BeatBoxActivity extends SingleActivity {

    @Override
    protected Fragment createFragment() {
        return BeatBoxFragment.newInstance();
    }
}

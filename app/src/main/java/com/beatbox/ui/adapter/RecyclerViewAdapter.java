package com.beatbox.ui.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class RecyclerViewAdapter<T> extends RecyclerView.Adapter<RecyclerViewAdapter.BindingHolder> {
    @LayoutRes
    private int layoutResourceId;
    private int viewHolderDataBindingId;
    private List<T> items = new ArrayList<>();

    public RecyclerViewAdapter(int layoutResourceId, int viewHolderDataBindingId, List<T> items) {
        this.layoutResourceId = layoutResourceId;
        this.viewHolderDataBindingId = viewHolderDataBindingId;
        this.items = items;
    }

    @Override
    public BindingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(layoutResourceId, parent, false);
        return new BindingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingHolder holder, int position) {
        T item = items.get(position);
        holder.getBinding().setVariable(viewHolderDataBindingId, item);
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    protected static class BindingHolder extends RecyclerView.ViewHolder {
        @Getter
        private ViewDataBinding binding;

        protected BindingHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }
    }
}

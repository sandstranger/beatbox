package com.beatbox.ui.fragment;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import lombok.Getter;

abstract class SingleFragment <T extends ViewDataBinding> extends Fragment {
    @Getter
    private T binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutResId(), container, false);
        configureDagger();
        bindView();
        return binding.getRoot();
    }

    @LayoutRes
    protected abstract int getLayoutResId();
    protected abstract void bindView();
    protected abstract void configureDagger ();
}

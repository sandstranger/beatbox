package com.beatbox.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.beatbox.beatbox.R;
import com.beatbox.beatbox.databinding.FragmentBeatBoxBinding;
import com.beatbox.interfaces.components.DaggerBeatBoxComponent;
import com.beatbox.model.BeatBox;
import com.beatbox.modules.ContextModule;
import com.beatbox.ui.view.BeatBoxFragmentView;

import lombok.val;

public class BeatBoxFragment extends SingleFragment<FragmentBeatBoxBinding> {
    private BeatBox beatBox;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_beat_box;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    protected void bindView() {
        val view = BeatBoxFragmentView.builder().context(getContext()).beatBox(beatBox).binding(getBinding()).build();
        view.bindView();
    }

    @Override
    protected void configureDagger() {
        beatBox = DaggerBeatBoxComponent.builder().contextModule(new ContextModule(getActivity())).build().getBeatBox();
    }

    public static BeatBoxFragment newInstance() {
        return new BeatBoxFragment();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        beatBox.release();
    }
}

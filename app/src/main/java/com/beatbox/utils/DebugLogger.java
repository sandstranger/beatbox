package com.beatbox.utils;

import android.support.annotation.Nullable;
import android.util.Log;

import com.beatbox.beatbox.BuildConfig;

public final class DebugLogger {
    private static final String APP_TAG = "BeatBox";
    private static final String EXCEPTION_TAG = "exception";

    private DebugLogger() {

    }

    public static void logMessage(String message) {
        if (debugBuild()) {
            Log.d(APP_TAG, message);
        }
    }

    public static void logException(@Nullable String message, Exception e) {
        if (debugBuild()) {
            if (message == null || message.isEmpty()) {
                message = EXCEPTION_TAG;
            }
            Log.e(APP_TAG, message, e);
        }
    }

    private static boolean debugBuild() {
        return BuildConfig.DEBUG;
    }
}

package com.beatbox.utils;

public final class Constants {
    public static final String ASSETS_SOUNDS_FOLDER = "sample_sounds";

    private Constants (){

    }
}

package com.beatbox.utils;

public class FileUtils {

    public static String getFileNameFromPath(String filePath) {
        String name = "";
        try {
            String[] components = filePath.split("/");
            name = components[components.length - 1];
        } catch (Exception e) {
            DebugLogger.logException("",e);
        }
        return name;
    }
}

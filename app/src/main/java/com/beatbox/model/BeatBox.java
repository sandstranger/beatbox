package com.beatbox.model;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import com.beatbox.utils.DebugLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.val;

import static com.beatbox.utils.Constants.ASSETS_SOUNDS_FOLDER;

public class BeatBox {
    private static final int MAX_SOUNDS_COUNT_FOR_POOL = 5;
    private AssetManager assetManager;
    @Getter
    private List<Sound> sounds = new ArrayList<>();
    private SoundPool soundPool = new SoundPool(MAX_SOUNDS_COUNT_FOR_POOL, AudioManager.STREAM_MUSIC, 0);

    public BeatBox(Context context) {
        assetManager = context.getAssets();
        loadSounds();
    }

    private void loadSounds() {
        try {
            val soundsNames = getSoundsNames();
            DebugLogger.logMessage("Found " + soundsNames.length + " sounds");
            for (val soundName : soundsNames) {
                buildSound(soundName);
            }
        } catch (Exception e) {
            DebugLogger.logException("Sounds not found", e);
        }
    }

    private String[] getSoundsNames() throws IOException {
        return assetManager.list(ASSETS_SOUNDS_FOLDER);
    }

    private void buildSound(String soundName) throws IOException {
        val assetPath = ASSETS_SOUNDS_FOLDER + "/" + soundName;
        val sound = new Sound(assetPath);
        loadSound(sound);
        sounds.add(sound);
    }

    private void loadSound(Sound sound) throws IOException {
        AssetFileDescriptor fileDescriptor = assetManager.openFd(sound.getAssetPath());
        sound.setSoundId(soundPool.load(fileDescriptor, 1));
    }

    public void playSound(Sound sound) {
        val soundId = sound.getSoundId();
        if (soundId != null) {
            soundPool.play(soundId, 1.0f, 1.0f, 1, 0, 1.0f);
        }
    }

    public void release() {
        soundPool.release();
    }
}

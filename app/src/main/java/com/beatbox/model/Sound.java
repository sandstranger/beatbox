package com.beatbox.model;

import com.beatbox.utils.FileUtils;

import lombok.Getter;
import lombok.Setter;

public class Sound {
    @Getter
    private String assetPath;
    @Getter
    private String name;
    @Getter
    @Setter
    private Integer soundId;

    public Sound (String assetPath){
        this.assetPath = assetPath;
        name = FileUtils.getFileNameFromPath(assetPath);
    }
}
